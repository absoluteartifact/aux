package com.AuxMusicShare

import android.app.AlertDialog
import android.content.ContentValues
import android.content.Context
import android.content.DialogInterface
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.format.DateUtils
import android.util.Base64
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.google.android.material.slider.RangeSlider
import java.io.File
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.properties.Delegates

private const val MAX_DURATION_IN_MS = 30000f

class SongMumboJumbo : AppCompatActivity() {
    //Global vars
    private lateinit var documentUri: Uri

    private lateinit var mediaPlayer: MediaPlayer

    private lateinit var rangeSlider: RangeSlider

    private lateinit var btnCheckLyrics: Button
    private lateinit var btnContinueSongMumboJumbo: Button
    private lateinit var btnPlayButton: Button
    private lateinit var btnPauseButton: Button
    private lateinit var btnStopButton: Button

    private lateinit var lblSongTitleActivityMumboJumbo: TextView
    private lateinit var lblArtistAlbumSongMumboJumbo: TextView

    private lateinit var lblSongStart: TextView
    private lateinit var lblDuration: TextView
    private lateinit var lblSelectionStartTime: TextView
    private lateinit var lblSelectionEndTime: TextView
    private lateinit var lblTimeDifference: TextView

    private var songDurationInMilliseconds by Delegates.notNull<Int>()
    private var songDurationInMillisecondsFloat by Delegates.notNull<Float>()
    private var songDurationInSecondsFloat by Delegates.notNull<Float>()

    private var sliderValuesDifference by Delegates.notNull<Float>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_mumbo_jumbo)

        getMediaInfo()
        initializeScreenElements()
        initializeRangeSliderAttributes()
        displayMediaInfo()
        addButtonListeners()
    }

    private fun saveToDatabase() {
        val dbHelper = SongDbHelper(applicationContext)
        val db = dbHelper.writableDatabase

        val currentDateAndTime = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Date())
        val title = getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_TITLE, "FAILED TO GET TITLE!")
        val album = getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_ALBUM, "FAILED TO GET ALBUM!")
        val artist = getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_ARTIST, "FAILED TO GET ARTIST!")
        val genre = getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_GENRE, "N/A")
        val albumArt = getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_ALBUMART_PATH, "FAILED TO GET ALBUM ART!")

        val values = ContentValues().apply {
            put(SongDbHelper.Song.COLUMN_NAME_DATETIME, currentDateAndTime)
            put(SongDbHelper.Song.COLUMN_NAME_TITLE, title)
            put(SongDbHelper.Song.COLUMN_NAME_ALBUM, album)
            put(SongDbHelper.Song.COLUMN_NAME_ARTIST, artist)
            put(SongDbHelper.Song.COLUMN_NAME_GENRE, genre)
            put(SongDbHelper.Song.COLUMN_NAME_ALBUM_ART, albumArt)
            //getstring sanitizes input?
            put(SongDbHelper.Song.COLUMN_NAME_SONG_START, lblSelectionStartTime.text.toString())
            put(SongDbHelper.Song.COLUMN_NAME_SONG_END, lblSelectionEndTime.text.toString())
        }

        Toast.makeText(applicationContext, "Time: $currentDateAndTime\nTitle: $title\nAlbum: $album\nArtist: $artist\nGenre: $genre\nAlbum Art Path: $albumArt", Toast.LENGTH_LONG).show()

        val newRowId = db?.insert(SongDbHelper.Song.TABLE_NAME, null, values)
    }

    /**
     * Obtains the URI for the file from the appropriate SharedPreferences key/value pair, and sets it on a global variable. It does not handle non-existing values.
     */
    private fun getMediaInfo() {
        //Sets global variable documentUri for usage
        getSharedPreferences(TAG, Context.MODE_PRIVATE).let { sharedPreferences ->
            if (sharedPreferences.contains(LAST_OPENED_URI_KEY)) {
                documentUri = sharedPreferences.getString(LAST_OPENED_URI_KEY, null)?.toUri()?: return@let
            }
        }

        //Starts MediaPlayer
        createMediaPlayer(documentUri)

        songDurationInMilliseconds = mediaPlayer.duration
        songDurationInMillisecondsFloat = songDurationInMilliseconds.toFloat()
        songDurationInSecondsFloat = songDurationInMilliseconds.toFloat()/1000
    }

    /**
     * Initializes multiple global variables referring to labels and buttons, and sets the main TextViews as marquees.
     */
    private fun initializeScreenElements() {
        //Sets labels
        lblSongStart = findViewById(R.id.lblSongStart)
        lblDuration = findViewById(R.id.lblEndTime)
        lblSelectionStartTime = findViewById(R.id.lblSelectedStart)
        lblSelectionEndTime = findViewById(R.id.lblSelectedEnd)
        lblTimeDifference = findViewById(R.id.lblTimeDifference)
        lblSongTitleActivityMumboJumbo = findViewById(R.id.lblSongTitle_ActivityMumboJumbo)
        lblArtistAlbumSongMumboJumbo = findViewById(R.id.lblArtistAlbum_SongMumboJumbo)

        //Overwrites placeholder text
        lblArtistAlbumSongMumboJumbo.text = getString(R.string.AlbumArtistPlaceholder, "Album", "Artist")

        //Initializes buttons
        btnPlayButton = findViewById(R.id.btnPlayButton)
        btnPauseButton = findViewById(R.id.btnPauseButton)
        btnStopButton = findViewById(R.id.btnStopButton)
        btnContinueSongMumboJumbo = findViewById(R.id.btnContinue_SongMumboJumbo)
        btnCheckLyrics = findViewById(R.id.btnCheckLyrics)

        marqueeTextViews()
    }

    /**
     * Initializes the global RangeSlider object, sets its maximum value to the song length, formats its labels, sets the values to 0 and 30 seconds by default, fills the selection time labels and adds an onChangeListener to it.
     */
    private fun initializeRangeSliderAttributes() {
        rangeSlider = findViewById(R.id.rangeSlider)
        rangeSlider.valueTo = songDurationInMillisecondsFloat

        rangeSlider.setLabelFormatter { value ->
            formatSecondsToHHMMSS(value)
        }

        //Sets values to create two sliders things
        if (songDurationInMillisecondsFloat < MAX_DURATION_IN_MS)
        {
            rangeSlider.values = listOf(0f, songDurationInMillisecondsFloat)
        }
        else
        {
            rangeSlider.values = listOf(0f, MAX_DURATION_IN_MS)
        }

        setSliderValuesDifference() //MOVE OUT OF HERE

        fillSelectionTimeLabels()
        addRangeSliderRangeLimiter()
        addRangeSliderSeeker()
    }

    private fun setSliderValuesDifference() {
        sliderValuesDifference = rangeSlider.values[1] - rangeSlider.values[0]
    }

    private fun displayMediaInfo() {
        lblSongTitleActivityMumboJumbo.text = getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_TITLE, "FAILED TO GET TITLE")
        lblArtistAlbumSongMumboJumbo.text = getString(R.string.AlbumArtistPlaceholder, getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_ALBUM, "FAILED TO GET ALBUM"), getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_ARTIST, "FAILED TO GET ARTIST"))

        lblDuration.text = formatSecondsToHHMMSS(songDurationInMillisecondsFloat)

        val imgAlbumArt = findViewById<ImageView>(R.id.imgAlbumArt)
        /*if(getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_ALBUMART_BASE64, getString(R.string.Base64ErrorImage)) != null) {
            val base64AlbumArt = Base64.decode(getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_ALBUMART_BASE64, getString(R.string.Base64ErrorImage)), Base64.NO_WRAP)
            val albumArtBitmap = BitmapFactory.decodeByteArray(base64AlbumArt, 0, base64AlbumArt.size)
            imgAlbumArt.setImageBitmap(albumArtBitmap)
        }
        else {
            imgAlbumArt.setImageResource(R.drawable.ic_baseline_album_24)
        }*/
        val albumArtLocalFile = File(getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(
            SONG_ALBUMART_PATH, "Make me a real file, one that says no album art available"))
        val albumArtBitmap = BitmapFactory.decodeByteArray(albumArtLocalFile.readBytes(), 0, albumArtLocalFile.length().toInt())
        imgAlbumArt.setImageBitmap(albumArtBitmap)
    }

    private fun addButtonListeners() {
        btnPlayButton.setOnClickListener {
            playSong(rangeSlider.values[0].toInt())
        }
        btnPauseButton.setOnClickListener {
            pauseSong()
        }
        btnStopButton.setOnClickListener {
            stopSong()
        }
        btnContinueSongMumboJumbo.setOnClickListener {
            saveToDatabase()
            /*val lyricsInputDialog: AlertDialog? = this?.let {
                val builder = AlertDialog.Builder(it)
                builder.apply {
                    setNegativeButton("Exit",
                        DialogInterface.OnClickListener { dialog, id ->
                            // User cancelled the dialog
                        })
                    setMessage("Please input lyrics...")
                    setView(R.layout.activity_lyrics_popup)
                    show()
                }
                // Set other dialog properties
                // Create the AlertDialog
                builder.create()
            }*/
        }
        btnCheckLyrics.setOnClickListener {
            val lyricsShow: AlertDialog? = this?.let {
                val builder = AlertDialog.Builder(it)
                builder.apply {
                    setPositiveButton(R.string.OK,
                        DialogInterface.OnClickListener { dialog, id ->
                        })
                    setNegativeButton(R.string.Cancel,
                        DialogInterface.OnClickListener { dialog, id ->
                            // User cancelled the dialog
                        })
                    setMessage(getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_LYRICS, getString(R.string.NoLyricsFound)))
                    show()
                }
                // Set other dialog properties
                // Create the AlertDialog
                builder.create()
            }
        }
    }

    private fun playSong(startTimeInMilliseconds: Int) {
        mediaPlayer.seekTo(startTimeInMilliseconds)
        mediaPlayer.start()
        //do this to pause at end for replaying, plays only for as long as the rangeslider allows
        val handlerForAutoPause = Handler()
        handlerForAutoPause.postDelayed(pauseAtEndForReplay, sliderValuesDifference.toLong()) //Sends the difference between thumbs to set the playtime, //seek back to original time
    }

    private fun pauseSong() {
        if (mediaPlayer.isPlaying) { //MODIFY TO MAKE TAPPABLE AGAIN TO RESUME PLAY!!!!!!!
            mediaPlayer.pause()
        }
    }

    var pauseAtEndForReplay = Runnable { mediaPlayer.pause();  }

    private fun stopSong() { // Maybe substitute with a pause?
        if (mediaPlayer.isPlaying) {
            releaseMediaPlayer(documentUri)
        }
    }

    private fun releaseMediaPlayer(documentUri: Uri) {
        mediaPlayer.stop()
        mediaPlayer.reset()
        createMediaPlayer(documentUri) //recreates MP for later usage
    }

    private fun addRangeSliderRangeLimiter() {
        rangeSlider.addOnChangeListener(RangeSlider.OnChangeListener { slider, currentThumbValue, fromUser ->

            /*if (rangeSlider.activeThumbIndex == 0) { //Gets active thumb index (IS IT THE SLIDER??? WHCICH ONE OF THE BALLSIES?)
                playSong(slider.values[0].toInt())
            }*/

            //sets playtime as current play... times
            //lblSongStart.text = formatSecondsToHHMMSS(mediaPlayer.currentPosition.toFloat()/1000) //check how to make this run while playing

            setSliderValuesDifference()

            if (sliderValuesDifference > MAX_DURATION_IN_MS) {
                if (fromUser)
                {
                    when (rangeSlider.activeThumbIndex) {
                        0 -> { //currentThumbValue is slider ball 1's
                            slider.values = listOf(currentThumbValue, currentThumbValue + MAX_DURATION_IN_MS) //SLIDERS MUST BE ASSIGNED BY ARRAYS, ELSE THEY WILL NOT GET THE VALUE
                        }
                        1 -> { //currentThumbValue is slider ball 2's
                            slider.values = listOf(currentThumbValue - MAX_DURATION_IN_MS, currentThumbValue)
                        }
                    }
                }
            }

            fillSelectionTimeLabels()
        })
    }

    private fun addRangeSliderSeeker() { //Could be substituted to an ontouch for arrival (pause) and release (seek)
        rangeSlider.addOnChangeListener(RangeSlider.OnChangeListener { slider, currentThumbValue, fromUser ->
            if (fromUser)
            {
                when(rangeSlider.activeThumbIndex) {
                    0, 1-> {
                        pauseSong()
                        mediaPlayer.seekTo(slider.values[0].toInt())
                    }
                }
            }
        })
    }

    //REFACTOR TO USE ONLY MS


    /**
     * Fills the tiny labels below the RangeSlider that indicate the current selected times, with the values from the RangeSlider value array.
     */
    private fun fillSelectionTimeLabels() {
        lblSelectionStartTime.text = formatSecondsToHHMMSS(rangeSlider.values[0])
        lblSelectionEndTime.text = formatSecondsToHHMMSS(rangeSlider.values[1])
        lblTimeDifference.text = formatSecondsToHHMMSS(sliderValuesDifference)
    }

    /**
     * Creates a new MediaPlayer object on the global variable.
     * @param documentUri receives the URI for the file from which to generate the MediaPlayer.
     */
    private fun createMediaPlayer(documentUri: Uri) {
        mediaPlayer = MediaPlayer.create(applicationContext, documentUri)
    }

    /**
     * Calls setTextViewAsSelected to enable marquee-ness (it has to be enabled with android:ellipsize="marquee" on the XML doc.
     */
    private fun marqueeTextViews() {
        setTextViewAsSelected(R.id.lblSongTitle_ActivityMumboJumbo)
        setTextViewAsSelected(R.id.lblArtistAlbum_SongMumboJumbo)
    }

    /**
     * Sets a TextView as active (for example, for making it scrollable/marqueeable)
     *@param textViewId receives a TextView ID (the one with the R.id.yaddayadda) and sets it as active
     */
    private fun setTextViewAsSelected(textViewId: Int) {
        findViewById<TextView>(textViewId).isSelected = true
    }

    /**
    * Converts whatever floating point value (in seconds!) is received to the typical HH:MM:SS format.
    *@param seconds receives a Float value. It's pretty self-explainatory.
    */
    private fun formatSecondsToHHMMSS(milliseconds: Float): String { //Make template function that receives ints (for milliseconds and local conversion), and floats (for slider data)
        return DateUtils.formatElapsedTime(milliseconds.toLong()/1000).toString()
    }

    /**
    * Does the code inside when the activity is destroyed. In this case it stops the song.
    */
    override fun onDestroy() {
        super.onDestroy()
        stopSong()
    }
}

private const val TAG = "MainActivity"
private const val LAST_OPENED_URI_KEY = "com.AuxMusicShare.pref.LAST_OPENED_URI_KEY"
private const val SONG_TITLE = "com.AuxMusicShare.pref.SONG_TITLE"
private const val SONG_ARTIST = "com.AuxMusicShare.pref.SONG_ARTIST"
private const val SONG_ALBUM = "com.AuxMusicShare.pref.SONG_ALBUM"
private const val SONG_GENRE = "com.AuxMusicShare.pref.SONG_GENRE"
private const val SONG_ALBUMART_BASE64 = "com.AuxMusicShare.pref.SONG_ALBUMART_BASE64"
private const val SONG_ALBUMART_PATH = "com.AuxMusicShare.pref.SONG_ALBUMART_PATH"
private const val SONG_PATH = "com.AuxMusicShare.pref.SONG_PATH"
private const val SONG_LYRICS = "com.AuxMusicShare.pref.SONG_LYRICS"