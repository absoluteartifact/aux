package com.AuxMusicShare

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.media.MediaMetadataRetriever
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.edit

class LyricsPopup : AppCompatActivity() {

    private lateinit var txtMultilineLyrics: TextView
    private lateinit var btnDoneLyrics: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lyrics_popup)

        txtMultilineLyrics = findViewById(R.id.txtMultilineLyrics)

        btnDoneLyrics = findViewById(R.id.btnDoneLyrics)

        val lyrics = txtMultilineLyrics.text.toString()


        btnDoneLyrics.setOnClickListener {
            getSharedPreferences(TAG, Context.MODE_PRIVATE).edit {
                putString(SONG_LYRICS, lyrics)
            }
            Toast.makeText(applicationContext, "Inputted text $lyrics", Toast.LENGTH_LONG)
        }
    }
}
private const val TAG = "MainActivity"
private const val SONG_LYRICS = "com.AuxMusicShare.pref.SONG_LYRICS"