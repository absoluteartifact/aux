package com.AuxMusicShare

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory.decodeByteArray
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.core.net.toFile
import androidx.core.net.toUri
import io.flutter.embedding.android.FlutterActivity
import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.tag.Tag
import java.io.File
import java.math.BigInteger
import java.net.URI
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class MainActivity : AppCompatActivity() {

    var songIsLoaded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnLoadSong = findViewById<Button>(R.id.btnLoadSong)
        val btnContinue = findViewById<Button>(R.id.btnContinue)
        val btnSongHistory = findViewById<Button>(R.id.btnHistory)

        if (songIsLoaded) //possibly useless
        {
            btnContinue.visibility = Button.VISIBLE
        }

        //Get elements on screen
        btnLoadSong.setOnClickListener {
            openDocumentPicker()
        }

        btnContinue.setOnClickListener {
            if (songIsLoaded) {
                val intent = Intent(this, SongMumboJumbo::class.java)
                startActivity(intent)
            }
            else {
                Toast.makeText(applicationContext, "Please load a song first!", Toast.LENGTH_SHORT).show()
            }

        }

        btnSongHistory.setOnClickListener {
            startActivity(FlutterActivity.createDefaultIntent(this))
        }

        overwriteTemplateText()

        getSharedPreferences(TAG, Context.MODE_PRIVATE).let { sharedPreferences ->
            if (sharedPreferences.contains(LAST_OPENED_URI_KEY)) {
                val documentUri =
                    sharedPreferences.getString(LAST_OPENED_URI_KEY, null)?.toUri() ?: return@let
                openDocument(documentUri)
            }
        }
    }

    private fun overwriteTemplateText() {
        findViewById<TextView>(R.id.lblAlbumArtist).text = Html.fromHtml(getString(R.string.AlbumArtistPlaceholder, "Album", "Artist"))
    }

    private fun marqueeTextViews() {
        setTextViewAsSelected(R.id.lblSongTitle)
        setTextViewAsSelected(R.id.lblAlbumArtist)
    }

    /**
     * Sets a TextView as active (for example, for making it scrollable/marqueeable)
     *@param textViewId receives a TextView ID (the one with the R.id.yaddayadda) and sets it as active
     */
    private fun setTextViewAsSelected(textViewId: Int) {
        findViewById<TextView>(textViewId).isSelected = true
    }


    private fun openDocumentPicker() {
        val mimeTypes = arrayOf("audio/*", "audio/flac", "application/flac")
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            type = "*/*"
            putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            addCategory(Intent.CATEGORY_OPENABLE)
        }
        startActivityForResult(intent, OPEN_DOCUMENT_REQUEST_CODE)
    }

    private fun openDocument(documentUri: Uri) {
        getSharedPreferences(TAG, Context.MODE_PRIVATE).edit {
            putString(LAST_OPENED_URI_KEY, documentUri.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultData)

        if (requestCode == OPEN_DOCUMENT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            resultData?.data?.also { documentUri ->
                contentResolver.takePersistableUriPermission(
                    documentUri,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
                getSharedPreferences(TAG, Context.MODE_PRIVATE).edit {
                    putString(SONG_PATH, documentUri.toString())
                }
                openDocument(documentUri)
                setSongData(documentUri)
                songIsLoaded = true
                findViewById<Button>(R.id.btnContinue).visibility = Button.VISIBLE

                marqueeTextViews()
            }
        }
        else {
            Toast.makeText(applicationContext, "Failed to load song.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setSongData(documentUri: Uri) {

        //Creates MediaMetadataRetriever to get song info
        val mmr = MediaMetadataRetriever()
        mmr.setDataSource(this, documentUri)

        val albumArtBytes = mmr.embeddedPicture
        val imgAlbumArt = findViewById<ImageView>(R.id.imgAlbum)

        getSharedPreferences(TAG, Context.MODE_PRIVATE).edit {
            putString(SONG_TITLE, mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)) //gets song info and sets it on a key/value pair
            putString(SONG_ALBUM, mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM))
            putString(SONG_ARTIST, mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST))
            putString(SONG_GENRE, mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE))
        }

        if (albumArtBytes != null) { //WILL BREAK IF DOESN'T HAVE A FALLBACK IMAGE, CREATE ONE ON APP FIRST LAUNCH AND USE THAT

            val albumArtLocalFile = File(applicationContext.filesDir, getFileMD5(albumArtBytes)) // Move saving to when song is actually saved
            if(!albumArtLocalFile.exists()) {
                albumArtLocalFile.writeBytes(albumArtBytes)
            }

           /* getSharedPreferences(TAG, Context.MODE_PRIVATE).edit() {
                putString(SONG_ALBUMART_BASE64, Base64.encodeToString(albumArtBytes, Base64.NO_WRAP))
            }
            val albumArtBitmap = BitmapFactory.decodeByteArray(albumArtBytes, 0, albumArtBytes.size)
            val imgAlbumArt = findViewById<ImageView>(R.id.imgAlbum)
            imgAlbumArt.setImageBitmap(albumArtBitmap)*/

            getSharedPreferences(TAG, Context.MODE_PRIVATE).edit() {
                putString(SONG_ALBUMART_PATH, albumArtLocalFile.path)
            }
            imgAlbumArt.setImageBitmap(decodeByteArray(albumArtLocalFile.readBytes(), 0, albumArtLocalFile.length().toInt()))
        }
        else
        {
            val blankAlbumPic = resources.openRawResource(R.raw.ic_baseline_album).readBytes()
            val albumArtLocalFile = File(applicationContext.filesDir, getFileMD5(blankAlbumPic)) // Move saving to when song is actually saved
            if(!albumArtLocalFile.exists()) {
                albumArtLocalFile.writeBytes(blankAlbumPic)
            }

            getSharedPreferences(TAG, Context.MODE_PRIVATE).edit() {
                putString(SONG_ALBUMART_PATH, albumArtLocalFile.path)
            }
            /*getSharedPreferences(TAG, Context.MODE_PRIVATE).edit() {
                putString(SONG_ALBUMART_BASE64, null)
            }*/

            imgAlbumArt.setImageBitmap(decodeByteArray(albumArtLocalFile.readBytes(), 0, albumArtLocalFile.length().toInt()))

        }

        val songTitle = getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_TITLE, "FAILED TO RETRIEVE SONG TITLE")
        val songAlbum = getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_ALBUM, "FAILED TO RETRIEVE SONG ALBUM")
        val songArtist = getSharedPreferences(TAG, Context.MODE_PRIVATE).getString(SONG_ARTIST, "FAILED TO RETRIEVE SONG ARTIST")

        findViewById<TextView>(R.id.lblSongTitle).text = songTitle
        findViewById<TextView>(R.id.lblAlbumArtist).text = getString(R.string.AlbumArtistPlaceholder, songAlbum, songArtist)
    }

    private fun getFileMD5(albumArtIntegratedFile: ByteArray): String {
        var mdEnc: MessageDigest? = null
        try {
            mdEnc = MessageDigest.getInstance("MD5")
        } catch (e: NoSuchAlgorithmException) {
            println("Exception while encrypting to md5")
            e.printStackTrace()
        } // Encryption algorithm

        mdEnc!!.update(albumArtIntegratedFile, 0, albumArtIntegratedFile.size)
        var md5: String = BigInteger(1, mdEnc.digest()).toString(16)
        while (md5.length < 32) {
            md5 = "0$md5"
        }
        return md5
    }
}

private const val OPEN_DOCUMENT_REQUEST_CODE = 0x33
private const val TAG = "MainActivity"
private const val LAST_OPENED_URI_KEY = "com.AuxMusicShare.pref.LAST_OPENED_URI_KEY"
private const val SONG_TITLE = "com.AuxMusicShare.pref.SONG_TITLE"
private const val SONG_ARTIST = "com.AuxMusicShare.pref.SONG_ARTIST"
private const val SONG_ALBUM = "com.AuxMusicShare.pref.SONG_ALBUM"
private const val SONG_GENRE = "com.AuxMusicShare.pref.SONG_GENRE"
private const val SONG_ALBUMART_BASE64 = "com.AuxMusicShare.pref.SONG_ALBUMART_BASE64"
private const val SONG_ALBUMART_PATH = "com.AuxMusicShare.pref.SONG_ALBUMART_PATH"
private const val SONG_PATH = "com.AuxMusicShare.pref.SONG_PATH"
private const val SONG_LYRICS = "com.AuxMusicShare.pref.SONG_LYRICS"
