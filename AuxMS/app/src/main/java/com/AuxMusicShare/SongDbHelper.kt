package com.AuxMusicShare

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

class SongDbHelper (context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION){

    object Song : BaseColumns {
        const val TABLE_NAME = "songHistory"
        const val COLUMN_NAME_DATETIME = "_dateTime"
        const val COLUMN_NAME_TITLE = "title"
        const val COLUMN_NAME_ALBUM = "album"
        const val COLUMN_NAME_ARTIST = "artist"
        const val COLUMN_NAME_GENRE = "genre"
        const val COLUMN_NAME_ALBUM_ART = "albumArt"
        const val COLUMN_NAME_SONG_START = "songStartTime"
        const val COLUMN_NAME_SONG_END = "songEndTime"
    }
    private val SQL_CREATE_ENTRIES =
        "CREATE TABLE ${Song.TABLE_NAME} (" +
                "${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT," +
                "${Song.COLUMN_NAME_DATETIME} TEXT," +
                "${Song.COLUMN_NAME_TITLE} TEXT," +
                "${Song.COLUMN_NAME_ALBUM} TEXT," +
                "${Song.COLUMN_NAME_ARTIST} TEXT," +
                "${Song.COLUMN_NAME_GENRE} TEXT," +
                "${Song.COLUMN_NAME_ALBUM_ART} TEXT," +
                "${Song.COLUMN_NAME_SONG_START} TEXT," +
                "${Song.COLUMN_NAME_SONG_END} TEXT)"

    private val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS ${Song.TABLE_NAME}"

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    companion object {
        const val DATABASE_VERSION = 3
        const val DATABASE_NAME = "aux.db"
    }
}