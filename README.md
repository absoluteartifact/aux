# Aux Music Sharer

This Android app is designed to help you share snippets of music you own, making use of local music files.

It is written in Kotlin and Flutter.

Its main use case is clipping up to 30 seconds of music to share on messaging apps like WhatsApp or Messenger, by making a video which includes the song\'s album art and relevant information (like artist, album and title).

Should your music files include metadata (artist, album, title, album art...), the app will automatically scrape it and use it to generate the video. You can also modify the data while generating the video, in case you don\'t tag your music.
