import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:song_history/editar.dart';
import 'classes/database.dart';
import 'package:holding_gesture/holding_gesture.dart';
import 'classes/cancion.dart';
import 'dart:io';

class Historial extends StatefulWidget {
  const Historial({Key? key}) : super(key: key);

  @override
  State createState() => _HistorialState();
}

class _HistorialState extends State<Historial> {
  List<Map<String, dynamic>> _canciones = [];
  bool themeDark = true;

  bool _isLoading = true;
  //Gets data from DB
  void _refreshSongs() async {
    final data = await DB.canciones();
    setState(() {
      _canciones = data.reversed.toList();
      _isLoading = false;
    });
  }

  void goToEdit(Cancion song) async {
    await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Editar(
                  song: song,
                )));
    _refreshSongs();
  }

  @override
  void initState() {
    super.initState();
    _refreshSongs(); //Load on activity launch
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /* appBar: AppBar(
        title: const Text('Song blue'),
      ), */
      backgroundColor: const Color(0xff1c1b1f),
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: _canciones.length,
              itemBuilder: (context, index) => HoldTimeoutDetector(
                  onTimeout: () {
                    goToEdit(Cancion(
                        index: _canciones[index]["_id"],
                        dateTime_: _canciones[index]["_dateTime"],
                        title: _canciones[index]["title"],
                        album: _canciones[index]["album"],
                        artist: _canciones[index]["artist"],
                        genre: _canciones[index]["genre"],
                        albumArt: _canciones[index]["albumArt"],
                        songStartTime: _canciones[index]["songStartTime"],
                        songEndTime: _canciones[index]["songEndTime"]));
                  },
                  holdTimeout: const Duration(milliseconds: 500),
                  enableHapticFeedback: true,
                  onTimerInitiated: () {},
                  child: Container(
                    decoration: _boxDecoration(context),
                    margin: const EdgeInsets.fromLTRB(7, 5, 7, 5),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: 150,
                            alignment: Alignment.centerLeft,
                            child:
                                Image.file(File(_canciones[index]['albumArt'])),
                            /*child: Image.memory(
                              base64Decode(_canciones[index]['albumArt']),
                            ),*/
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Text(
                                    _canciones[index]['title'],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: themeDark
                                            ? Colors.white
                                            : Colors.black,
                                        fontWeight: FontWeight.w900,
                                        fontSize: 21.0),
                                    overflow: TextOverflow.fade,
                                    softWrap: true,
                                  ),
                                ),
                                Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Text(
                                    _canciones[index]['artist'],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: themeDark
                                          ? Colors.white
                                          : Colors.black,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 17.0,
                                    ),
                                    overflow: TextOverflow.fade,
                                    softWrap: true,
                                  ),
                                ),
                                Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Text(
                                    _canciones[index]['album'],
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: themeDark
                                            ? Colors.white
                                            : Colors.black,
                                        fontWeight: FontWeight.w300,
                                        fontSize: 16.0),
                                    overflow: TextOverflow.fade,
                                    softWrap: true,
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          _canciones[index]['songStartTime'],
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: themeDark
                                                  ? Colors.white
                                                  : Colors.black,
                                              fontWeight: FontWeight.w300,
                                              fontSize: 16.0),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 0, 10, 0),
                                            child: Text(
                                              _canciones[index]['genre'],
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: themeDark
                                                      ? Colors.white
                                                      : Colors.black,
                                                  fontWeight: FontWeight.w300,
                                                  fontSize: 16.0),
                                            ),
                                          ),
                                        ),
                                        Text(
                                          _canciones[index]['songEndTime'],
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: themeDark
                                                  ? Colors.white
                                                  : Colors.black,
                                              fontWeight: FontWeight.w300,
                                              fontSize: 16.0),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                  child: Text(
                                    DateFormat('yyyy-MM-dd\nkk:ss').format(
                                        DateTime.parse(
                                            _canciones[index]['_dateTime'])),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: themeDark
                                            ? Colors.white
                                            : Colors.black,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 16.0),
                                    overflow: TextOverflow.fade,
                                    softWrap: true,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ))

              /* Card(
                color: Colors.teal,
                margin: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                child: ListTile(
                  contentPadding: const EdgeInsets.all(10),
                  title: Text(_canciones[index]['title']),
                  subtitle: Text(_canciones[index]['artist']),
                  leading: Container(
                    child: Image.memory(
                        base64Decode(_canciones[index]['albumArt'])),
                  ),
                  trailing: Container(
                    child: Text(_canciones[index]['_dateTime']),
                  ),
                ),
              ), */
              ),
    );
  }

  BoxDecoration _boxDecoration(context) {
    return BoxDecoration(
      color: themeDark ? Theme.of(context).primaryColor : Colors.white,
      borderRadius: BorderRadius.circular(10.0),
      /* boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.green, offset: Offset(2.0, 2.0), blurRadius: 3.0)
        ] */
    );
  }
}
