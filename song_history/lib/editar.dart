import 'dart:convert';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'classes/cancion.dart';
import 'classes/database.dart';
import 'dart:io';
import 'package:crypto/crypto.dart' as crypto;
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'package:path/path.dart' as flutterPath;

class Editar extends StatefulWidget {
  Editar({Key? key, required this.song}) : super(key: key);
  final String appbarTitle = "Edit song";

  final Cancion song;
  final String lblTitle = "Title",
      lblAlbum = "Album",
      lblArtist = "Artist",
      lblGenre = "Genre";
  final ImagePicker imagePicker = ImagePicker();

  @override
  State createState() => _EditarState();
}

class _EditarState extends State<Editar> {
  @override
  void initState() {
    _fillTextBoxes(); //Load on activity launch
    super.initState();
  }

  Color bgcolor = const Color(0xff1c1b1f);
  TextEditingController titleController = TextEditingController();
  TextEditingController albumController = TextEditingController();
  TextEditingController artistController = TextEditingController();
  TextEditingController genreController = TextEditingController();
/*   TextEditingController albumArtController = TextEditingController();
 */
  var songAlbumArt;

  void _fillTextBoxes() {
    titleController.text = widget.song.title;
    albumController.text = widget.song.album;
    artistController.text = widget.song.artist;
    genreController.text = widget.song.genre;
/*     albumArtController.text = widget.song.album;
 */
    songAlbumArt = widget.song.albumArt;
  }

  void deleteSong() async {
    //remove this and go back to the previous screen and delete it
    await DB.delete(widget.song);
    if (mounted) {
      Navigator.pop(context);
    }
  }

  Future<void> updateSong(Cancion song) async {
    await DB.update(song);
    if (mounted) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
          'Edited song succesfully.',
          style: Theme.of(context).textTheme.button,
        ),
        backgroundColor: Colors.purple,
      ));
      Navigator.pop(context);
    }
  }

  Future<void> pickImage() async {
    XFile? xFileImage =
        await widget.imagePicker.pickImage(source: ImageSource.gallery);
    if (xFileImage != null) {
      songAlbumArt =
          "/data/user/0/com.AuxMusicShare/files/${getFileMD5(xFileImage.path)}"; /*base64Encode(File(xFileImage.path).readAsBytesSync());*/
      File albumArtLocalPic = await File(xFileImage.path).copy(songAlbumArt);
    }
  }

  String getFileMD5(String filePath) {
    String md5OfFile;
    var md5method = crypto.md5;
    md5OfFile = md5method.convert(File(filePath).readAsBytesSync()).toString();
    return md5OfFile;
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      //Unfocuses and removes keyboard when unneeded
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: bgcolor,

        appBar: AppBar(
          title: Text(widget.appbarTitle),
          backgroundColor: Theme.of(context).primaryColor,
          actions: [
            IconButton(
              icon: const Icon(
                Icons.delete,
                color: Colors.white,
              ),
              tooltip: "Delete",
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      backgroundColor: bgcolor,
                      content: SizedBox(
                        height: 65,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Are you sure you want to delete this song?",
                                  style: Theme.of(context).textTheme.button,
                                )
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ElevatedButton(
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Theme.of(context).primaryColor)),
                                  onPressed: () {
                                    deleteSong();
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    "Yes",
                                    style: Theme.of(context).textTheme.button,
                                  ),
                                ),
                                ElevatedButton(
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Theme.of(context).primaryColor)),
                                  onPressed: () => Navigator.pop(context),
                                  child: Text(
                                    "No",
                                    style: Theme.of(context).textTheme.button,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ],
        ),

        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Form(
                  key: _formKey,
                  child: ListView(
                    children: [
                      SizedBox(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                                fillColor: Theme.of(context).highlightColor,
                                border: const UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 2,
                                      color: Colors.black,
                                    ),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                                filled: true,
                                labelText: widget.lblTitle,
                                labelStyle: const TextStyle(
                                    color: Color(0xFF385275),
                                    fontWeight: FontWeight.bold)),
                            controller: titleController,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please enter a title!";
                              }
                              if (RegExp(r"^\s+").hasMatch(value)) {
                                return "No whitespace allowed!";
                              }
                              if (value.length >= 1000000000) {
                                return "Text too long! (1,000,000,000 characters max, you put ${value.length}!";
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Theme.of(context).primaryColor)),
                            onPressed: () => {
                              titleController.text = widget.song.title,
                            },
                            child: Text(
                              'Reset title',
                              style: Theme.of(context).textTheme.button,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              fillColor: Theme.of(context).highlightColor,
                              border: const UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 2,
                                    color: Colors.black,
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              filled: true,
                              labelText: widget.lblAlbum,
                              labelStyle: const TextStyle(
                                  color: Color(0xFF385275),
                                  fontWeight: FontWeight.bold),
                            ),
                            controller: albumController,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please enter an album!";
                              }
                              if (RegExp(r"^\s+").hasMatch(value)) {
                                return "No whitespace allowed!";
                              }
                              if (value.length >= 1000000000) {
                                return "Text too long! (1,000,000,000 characters max, you put ${value.length}!";
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Theme.of(context).primaryColor)),
                            onPressed: () => {
                              albumController.text = widget.song.album,
                            },
                            child: Text(
                              'Reset album',
                              style: Theme.of(context).textTheme.button,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              fillColor: Theme.of(context).highlightColor,
                              border: const UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 2,
                                    color: Colors.black,
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              filled: true,
                              labelText: widget.lblArtist,
                              labelStyle: const TextStyle(
                                  color: Color(0xFF385275),
                                  fontWeight: FontWeight.bold),
                            ),
                            controller: artistController,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please enter an artist!";
                              }
                              if (RegExp(r"^\s+").hasMatch(value)) {
                                return "No whitespace allowed!";
                              }
                              if (value.length >= 1000000000) {
                                return "Text too long! (1,000,000,000 characters max, you put ${value.length}!";
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Theme.of(context).primaryColor)),
                            onPressed: () => {
                              artistController.text = widget.song.artist,
                            },
                            child: Text(
                              'Reset artist',
                              style: Theme.of(context).textTheme.button,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              fillColor: Theme.of(context).highlightColor,
                              border: const UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 2,
                                    color: Colors.black,
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              filled: true,
                              labelText: widget.lblGenre,
                              labelStyle: const TextStyle(
                                  color: Color(0xFF385275),
                                  fontWeight: FontWeight.bold),
                            ),
                            controller: genreController,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please enter a genre!";
                              }
                              if (RegExp(r"^\s+").hasMatch(value)) {
                                return "No whitespace allowed!";
                              }
                              if (value.length >= 1000000000) {
                                return "Text too long! (1,000,000,000 characters max, you put ${value.length}!";
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Theme.of(context).primaryColor)),
                            onPressed: () => {
                              genreController.text = widget.song.genre,
                            },
                            child: Text(
                              'Reset genre',
                              style: Theme.of(context).textTheme.button,
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15),
                        child: Image.file(File(songAlbumArt)),
                        /*child: Image.memory(base64Decode(songAlbumArt))*/
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Theme.of(context).primaryColor)),
                            onPressed: () => {
                              setState(
                                  () => songAlbumArt = widget.song.albumArt),
                            },
                            child: Text(
                              'Reset album art',
                              style: Theme.of(context).textTheme.button,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Theme.of(context).primaryColor)),
                            onPressed: () async {
                              await pickImage();
                              setState(() {});
                            },
                            child: Text(
                              'Pick image...',
                              style: Theme.of(context).textTheme.button,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            if (_formKey.currentState!.validate()) {
              widget.song.title = titleController.text;
              widget.song.album = albumController.text;
              widget.song.artist = artistController.text;
              widget.song.genre = genreController.text;
              widget.song.albumArt = songAlbumArt;
              await updateSong(widget.song);
            }
            /*else {
              print('Error');
            }*/
          },
          tooltip: 'Editar',
          backgroundColor: Theme.of(context).primaryColor,
          child: const Icon(
            Icons.check,
          ),
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
}
