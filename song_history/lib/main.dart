import 'package:song_history/historial.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Song history',
      theme: ThemeData(
        primaryColor: const Color(0xFFDE4978),
        highlightColor: const Color.fromARGB(255, 208, 170, 214),
        primarySwatch: Colors.blue,
        textTheme: const TextTheme(
          button: TextStyle(
            fontSize: 16,
            color: Colors.white,
            fontWeight: FontWeight.normal,
          ),
        ),
      ),
      home: const Historial() /* MyHomePage(title: 'Editar historial') */,
    );
  }
}
