import 'package:sqflite/sqflite.dart';
import 'cancion.dart';
import 'package:path/path.dart';

class DB {
  static Future<Database> _openDB() async {
    return openDatabase(join(await getDatabasesPath(), 'aux.db'), version: 3,
        onCreate: (db, version) async {
      await createTables(db);
    });
  }

  static Future<void> createTables(Database database) async {
    await database.execute(
      "CREATE TABLE \"songHistory\" ( \"_id\"	INTEGER PRIMARY KEY AUTOINCREMENT, \"_dateTime\"	TEXT, \"title\"	TEXT, \"album\"	TEXT, \"artist\"	TEXT, \"genre\"	TEXT, \"albumArt\"	TEXT, \"songStartTime\"	TEXT, \"songEndTime\"	TEXT)",
    );
  }

  static Future<Future<void>> insert(Cancion cancion) async {
    Database database = await _openDB();
    return database.insert("songHistory", cancion.toMap());
  }

  static Future<void> delete(Cancion cancion) async {
    Database database = await _openDB();
    database
        .delete("songHistory", where: "_id = ?", whereArgs: [cancion.index]);
  }

  static Future<int> update(Cancion cancion) async {
    Database database = await _openDB();
    return database.update("songHistory", cancion.toMap(),
        where: "_id = ?", whereArgs: [cancion.index]);
  }

  static Future<List<Map<String, dynamic>>> cancion(int id) async {
    final database = await _openDB();
    return database.query('songHistory',
        where: "_id = ?", whereArgs: [id], limit: 1);
  }

  static Future<List<Map<String, dynamic>>> canciones() async {
    Database database = await _openDB();
    /* final List<Map<String, dynamic>> cancionesMap =
        await database.query("songHistory"); */
    return database.query('songHistory',
        orderBy:
            "_id"); /* List.generate(
        cancionesMap.length,
        (i) => Cancion(
              index: cancionesMap[i]['_index'],
              dateTime_: cancionesMap[i]['_dateTime'],
              title: cancionesMap[i]['title'],
              album: cancionesMap[i]['album'],
              artist: cancionesMap[i]['artist'],
              genre: cancionesMap[i]['genre'],
              albumArt: cancionesMap[i]['albumArt'],
              songStartTime: cancionesMap[i]['songStartTime'],
              songEndTime: cancionesMap[i]['songEndTime'],
            )); */
  }
}
