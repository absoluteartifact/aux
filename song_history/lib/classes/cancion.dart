class Cancion {
  int index;
  String dateTime_;
  String title;
  String album;
  String artist;
  String genre;
  String albumArt;
  String songStartTime;
  String songEndTime;

  Cancion(
      {required this.index,
      required this.dateTime_,
      required this.title,
      required this.album,
      required this.artist,
      required this.genre,
      required this.albumArt,
      required this.songStartTime,
      required this.songEndTime});

  Map<String, dynamic> toMap() {
    return {
      '_id': index,
      '_dateTime': dateTime_,
      'title': title,
      'album': album,
      'artist': artist,
      'genre': genre,
      'albumArt': albumArt,
      'songStartTime': songStartTime,
      'songEndTime': songEndTime,
    };
  }
}
